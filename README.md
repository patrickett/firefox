# firefox
Current stylings for my firefox. Version 64.0.2

__Addons__
1. [Tree Style Tabs](https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/)
2. [Vimium-FF](https://addons.mozilla.org/en-US/firefox/addon/vimium-ff/)
3. [Dark Reader](https://addons.mozilla.org/en-US/firefox/addon/darkreader/)
4. [New Tab Homepage](https://addons.mozilla.org/en-US/firefox/addon/new-tab-homepage/)
5. [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/?src=search)

__New Tab Homepage__

I use the newtab homepage addon and [light-server](https://github.com/txchen/light-server)  which is a simple __nodejs__ server I run so that my newtab page is a custom local startpage. A function that normally is not possible in firefox for security reasons.

```npm install light-server ```

__uBlock Origin__

You can make simple blacklists that you can save to a file.

Also has a cool option of blocking certain elements. Right click an element and block it from your view.

__Tree Style Tabs__

It makes it easier to manage the 80 tabs I always have open.


__Files__

Everything in the chrome folder goes in your firefox profile. Explained in more detail [here](https://www.reddit.com/r/FirefoxCSS/comments/73dvty/tutorial_how_to_create_and_livedebug_userchromecss/).
